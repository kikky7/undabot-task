import './app.scss'

import axios from 'axios'
window.axios = axios

import Vue from 'vue';

Vue.config.productionTip = false

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import { routes } from './config/routes'

const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})

import App from './components/App.vue'

new Vue({
    el: '#app',
    render: h => h(App),
    data: {},
    router
})