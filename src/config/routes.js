import PageIndex from '../components/views/Index.vue'
import PageContact from '../components/views/Contact.vue'
import PageNotFound from '../components/views/NotFound.vue'

const routes = [
    {
        path: '/',
        name: 'index',
        component: PageIndex,
    },
    {
        path: '/contact',
        name: 'contact',
        component: PageContact,
    },
    {
        path: '/*',
        name: 'not-found',
        component: PageNotFound,
        alias: '/404'
    }
]

export { routes }