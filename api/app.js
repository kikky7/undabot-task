const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const { check, validationResult } = require('express-validator/check')

const app = express()
const port = 3000

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const router = express.Router()

app.options('*', cors())

router.post('/contact', [
    cors(),
    check('email')
        .isEmail()
        .withMessage('Must have a valid email address'),
    check('message')
        .isLength({ min: 31 })
        .withMessage('Message must be longer than 30 characters')
], (req, res) => {
    const errors = validationResult(req)

    if (! errors.isEmpty()) {
        console.log(errors.array())

        return res.status(422)
            .json({
                errors: errors.array()
            })
    }

    const email = req.body.email
    const message = req.body.message

    console.log(email)
    console.log(message)

    res.json('Your message has been sent')
})

app.use('/api', router)

app.listen(port, () => console.log(`API app listening on port ${port}!`))