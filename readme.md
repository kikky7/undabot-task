# Undabot - Frontend Recruitment test

### Local setup

```bash
git clone git@bitbucket.org:kikky7/undabot-task.git

# Go to app directory
cd undabot-task

# Install JS dependencies (node 10+, npm 6+)
npm install

# Compile required distribution files and start webpack development server at localhost:8080
npm run start

# Open new cmd/terminal tab/window and go to api app directory
cd api

# Install JS dependencies
npm install

# Run API app (localhost:3000)
node app.js

# Go to http://localhost:8080/contact and test contact form
```